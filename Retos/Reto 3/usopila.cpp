/**
 * @file usopila.cpp
 * @brief Fichero ejemplo de uso de la implementación del TDA Pila
 *
 */

#include <iostream>
#include "pila.h"

using namespace std;

int main() {

    cout << "\n###################################################" << endl;
    cout << "# TDA Pila basado en List de STL usando templates #" << endl;
    cout << "###################################################" << endl;

    Pila<int> p;

    cout << "\nTRABAJANDO CON UNA Pila DE ENTEROS: ";
    for (int i = 0; i < 10; i++) {
        p.poner(i);
        cout << "[" << i << "]";
    }
    cout << "\n----------------------------------" << endl;
    cout << "\t- Número de elementos en la Pila de enteros: " << p.num_elementos() << endl;
    cout << "\t- El elemento al top de nuestra Pila de enteros es: " << p.top() << endl;
    cout << "\t- Creando una copia de la Pila de enteros..." << endl;
    Pila<int> auxp;
    auxp = p;
    p.top() = 22;
    cout << "\t- Elemento al top de la Pila de enteros cambiado, ahora es: " << p.top() << endl;
    cout << "\t- Elemento al top de la copia de la Pila de enteros: " << auxp.top() << endl;
    p.quitar();
    cout << "\t- Quitado primer elemento de la Pila de enteros, el nuevo top de la Pila de enteros es: " << p.top() << endl;
    cout << "\t- Vaciando Pila de enteros..." << endl;
    p.limpiar();
    cout << "\t- Número de elementos en la Pila de enteros: " << p.num_elementos() << endl;

    Pila<float> q;
    float aux;

    cout << "\nTRABAJANDO CON UNA Pila DE FLOTANTES: ";
    for (int i = 0; i < 10; i++) {
        aux = i / 10.0;
        q.poner(aux);
        cout << "[" << aux << "]";
    }
    cout << "\n----------------------------------" << endl;
    cout << "\t- Número de elementos en la Pila de flotantes: " << q.num_elementos() << endl;
    cout << "\t- El elemento al top de nuestra Pila de flotantes es: " << q.top() << endl;
    cout << "\t- Creando una copia de la Pila de flotantes..." << endl;
    Pila<float> auxq;
    auxq = q;
    q.top() = 5.5;
    cout << "\t- Elemento al top de la Pila de flotantes cambiado, ahora es: " << q.top() << endl;
    cout << "\t- Elemento al top de la copia de la Pila de flotantes: " << auxq.top() << endl;
    q.quitar();
    cout << "\t- Quitado primer elemento de la Pila de flotantes, el nuevo top de la Pila de flotantes es: " << q.top() << endl;
    cout << "\t- Vaciando Pila de flotantes..." << endl;
    q.limpiar();
    cout << "\t- Número de elementos en la Pila de flotantes: " << q.num_elementos() << endl;

    Pila<char> r;
    char caux = 'a' - 1;

    cout << "\nTRABAJANDO CON UNA Pila DE CARACTERES: ";
    for (int i = 0; i < 10; i++) {
        caux += 1;
        r.poner(caux);
        cout << "[" << caux << "]";
    }
    cout << "\n----------------------------------" << endl;
    cout << "\t- Número de elementos en la Pila de caracteres: " << r.num_elementos() << endl;
    cout << "\t- El elemento al top de nuestra Pila de caracteres es: " << r.top() << endl;
    cout << "\t- Creando una copia de la Pila de caracteres..." << endl;
    Pila<char> auxr(r);
    r.top() = 'z';
    cout << "\t- Elemento al top de la Pila de caracteres cambiado, ahora es: " << r.top() << endl;
    cout << "\t- Elemento al top de la copia de la Pila de caracteres: " << auxr.top() << endl;
    r.quitar();
    cout << "\t- Quitado primer elemento de la Pila de caracteres, el nuevo top de la Pila de caracteres es: " << r.top() << endl;
    cout << "\t- Vaciando Pila de caracteres..." << endl;
    r.limpiar();
    cout << "\t- Número de elementos en la Pila de caracteres: " << r.num_elementos() << endl;

    cout << endl;

    return 0;
}
