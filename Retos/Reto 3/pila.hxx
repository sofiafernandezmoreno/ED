/**
 * @file pila.hxx
 * @brief Fichero implementación métodos del TDA Pila
 *
 */

#include <list>
#include <cassert>

using namespace std;

template<class T>
Pila<T>::Pila() {
    
}

template<class T>
Pila<T>::Pila(const Pila<T> &original) {
    
    this->lista = original.lista;
}

template<class T>
Pila<T>::~Pila() {
   
    this->lista.clear();
}

template<class T>
Pila<T>& Pila<T>::operator=(const Pila<T> &otra) {
    /// Implementamos el constructor de asignación como cualquier otro.
    if (this != &otra)
        this->lista = otra.lista;

    return *this;
}

template<class T>
bool Pila<T>::vacia() const {
    
    return this->lista.empty();
}

template<class T>
T& Pila<T>::top() {
    /// Nos aseguramos que nuestra Pila no está vacía, y devolvemos
    /// una referencia al primer elemento de la Pila.
    assert(!this->vacia());

    return this->lista.front();
}

template<class T>
const T& Pila<T>::top() const {
    /// Nos aseguramos que nuestra Pila no está vacía, y devolvemos
    /// una referencia constante al primer elemento de la Pila.
    assert(!this->vacia());

    return this->lista.front();
}

template<class T>
void Pila<T>::poner(const T &elem) {
    /// Usamos el método de List para poner un elemento al final.
    this->lista.push_back(elem);
}

template<class T>
void Pila<T>::quitar() {
    assert(!this->vacia());

    /// Usamos el método para sacar el primer elemento del objeto List.
    this->lista.pop_back();
}

template<class T>
void Pila<T>::limpiar() {
    this->lista.clear();
}

template<class T>
int Pila<T>::num_elementos() const {
    return this->lista.size();
}
