//Elementos comunes de dos conjuntos(INTERSECCION)
#include <set>
#include <iostream>

using namespace std;

void comunes(set<int> c1, set<int> c2, set<int> & resultado){
  set<int>::iterator p;

  for(p=c1.begin();p!=c1.end();++p){//Recorro el primer conjunto
    if(c2.count(*p)){//Compruebo que los elementos iterados se encuentran en c2
      resultado.insert(*p);//Inserto
    }
  }
}

int main(){
  set<int> c1,c2,conjuntofinal;

  for(int i =0 ;i<11;i++){
    c1.insert(i);
  }
  for(int j =0 ;j<10;j++){
    c2.insert(j);
  }


    comunes(c1,c2,conjuntofinal);
    cout<<"Conjunto final generado--> ";
    for(set<int>::iterator z =conjuntofinal.begin() ;z!=conjuntofinal.end();z++){

        cout<<*z;


    }
    cout<<endl;
  }
