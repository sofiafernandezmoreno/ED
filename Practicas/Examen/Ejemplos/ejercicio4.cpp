/*
Construir funcion para determinar si
un conjunto tiene más de la mitad de sus
elementos comunes con otro
*/

#include <set>
#include <iostream>

using namespace std;

bool masdelamitadcomunes(const set<int> & c1, const set<int> & c2){

  set<int>::const_iterator p;
  int i,j;
  i=0;
  j=c1.size();

  for(p=c1.begin();p!=c1.end();++p){//Recorro y compruebo que p está en c2
    if(c2.count(*p)){
      i++;//Voy avanzando
    }
  }
  return (i>j/2);//Compruebo si los elementos comprobados son mayores que el total

}

int main(){
  set<int> c1,c2;

  for(int i =0 ;i<11;i++){
    c1.insert(i);
  }
  for(int j =0 ;j<10;j++){
    c2.insert(j);
  }


  for(set<int>::iterator z =c1.begin() ;z!=c1.end();z++){

      cout<<*z;


  }
  cout<<endl;
  for(set<int>::iterator q =c2.begin() ;q!=c2.end();q++){

      cout<<*q;


  }
  cout<<endl;
  //masparesqueimpares(c1,c2,conjuntofinal);
  cout<<"¿Hay más pares que impares? "<<masdelamitadcomunes(c1,c2)<<endl;

}
