#include <set>
#include <iostream>

using namespace std;

//Comprueba que el numero de pares
//en el conjunto es mayor que el de impares
bool masparesqueimpares(const set<int> & c, set<int> & cpar, set<int> & cimpar){
  set<int>::iterator p;
  for(p=c.begin();p!=c.end();++p){//Recorro el conjunto
    if((*p)%2==0){
      cpar.insert(*p);
    }
    else{
      cimpar.insert(*p);
    }
  }
  return cpar.size() > cimpar.size();

}



  int main(){
    set<int> c1,c2,conjuntofinal;

    for(int i =0 ;i<17;i++){
      conjuntofinal.insert(i);
    }



    //masparesqueimpares(c1,c2,conjuntofinal);
    cout<<"¿Hay más pares que impares? "<<masparesqueimpares(c1,c2,conjuntofinal)<<endl;

}
