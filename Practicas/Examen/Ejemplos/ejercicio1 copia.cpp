#include <set>
#include <iostream>

using namespace std;

//Elementos no comunes de dos conjuntos(UNION)
set<int> no_comunes(set<int> c1, set<int> c2){
  set<int>::iterator p,q;
  set<int> solucion;
  //Recorro el primer conjunto
  for(p=c1.begin();p!=c1.end();++p){
    if(!c2.count(*p)){//Comprueba si no está p está en c2
      solucion.insert(*p);//inserto en solucion
    }
  }
  for(q=c2.begin();q!=c2.end();++q){
    if(!c1.count(*q)){//Comprueba si no está q está en c1
      solucion.insert(*q);//inserto en solucion
    }
  }
  return solucion;

}
int main(){
  set<int> c1,c2,conjuntofinal;

  for(int i =0 ;i<11;i++){
    c1.insert(i);
  }
  for(int j =0 ;j<10;j++){
    c2.insert(j);
  }


    conjuntofinal=no_comunes(c1,c2);
    for(set<int>::iterator z =conjuntofinal.begin() ;z!=conjuntofinal.end();z++){
      cout<<*z<<endl;
    }
  //cout<<"COnjunto SET FINAL---> " <<conjuntofinal<<endl;


}
