#include <set>
#include <iostream>

using namespace std;

/*
Construir una funcion para determinar
si un conjunto tiene todos los elementos
pares incluidos dentro de otro
*/

bool inclusionpares(const set<int> & c1, const set<int> & c2){

  set<int>::iterator p;
  int i,j;
  i=0;
  j=c1.size();

  for(p=c1.begin();p!=c1.end();++p){//Recorro y compruebo que p está en c2
    if((*p)%2==0){
      if(c2.find(*p)==c2.end()){
        return false;
      }
      else{
        ++p;
      }
    }
}
return true;
}


int main(){
  set<int> c1,c2;

  for(int i =0 ;i<10;i++){
    c1.insert(i);
  }
  for(int j =0 ;j<10;j++){
    c2.insert(j);
  }


  for(set<int>::iterator z =c1.begin() ;z!=c1.end();z++){

      cout<<*z;


  }
  cout<<endl;
  for(set<int>::iterator q =c2.begin() ;q!=c2.end();q++){

      cout<<*q;


  }
  cout<<endl;
  //masparesqueimpares(c1,c2,conjuntofinal);
  cout<<"¿Están incluidos los pares? "<<inclusionpares(c1,c2)<<endl;

}
