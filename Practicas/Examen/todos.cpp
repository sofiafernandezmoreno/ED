#include <iostream>
#include <set>
#include <ctime>
#include <cstdlib>

using namespace std;

/**
 * @brief Imprime un conjunto de tipo @c T sobre el flujo de salida
 * @param s flujo de salida. Es MODIFICADO
 * @param s conjunto con los elementos a imprimir
 */

 ostream& operator<<(ostream &os, const set<int> &s){
   set<int>::iterator i;
   os << "{ ";
   for(i= s.begin();i!= s.end(); i++)
    os << (*i) << ", ";

  os <<" }";

    return os;
  }
  /**
    * @brief Inicializa el generador de numero aleatorios
    * Esta función debe ser llamada antes de usar rand()
    */
    void InicializarSemilla(){
      srand(time(0));
    }

    /**
     * @brief Genera número enteros positivos aleatorios en el rango [min,max]
     */
    int GeneraEntero(int min, int max){
        int tam = max - min + 1;
        return ((rand() % tam) + min);
    }
/*
***********************UNION***********************
*/
void _union(const set<int> &c1,const set<int> &c2, set<int> &c){
  set<int>::iterator i,j;
  for ( i = c1.begin(); i != c1.end(); ++i) {
    c.insert(*i);
  }
  for ( j = c2.begin(); j != c2.end(); ++j) {
    c.insert(*j);
  }
}
/*
*****************INTERSECCION**********************
*/
void interseccion(const set<int> &c1,const set<int> &c2, set<int> &c){
  set<int>::iterator i;
  for ( i = c1.begin(); i != c1.end(); ++i) {
    if(c2.count(*i))
      c.insert(*i);
  }
}

/*
**********Elementos no comunes de dos conjuntos(DIFERENCIA SIMETRICA)***********
*/
set<int> no_comunes(set<int> c1, set<int> c2){
  set<int>::iterator p,q;
  set<int> solucion;
  //Recorro el primer conjunto
  for(p=c1.begin();p!=c1.end();++p){
    if(!c2.count(*p)){//Comprueba si no está p está en c2
      solucion.insert(*p);//inserto en solucion
    }
  }
  for(q=c2.begin();q!=c2.end();++q){
    if(!c1.count(*q)){//Comprueba si no está q está en c1
      solucion.insert(*q);//inserto en solucion
    }
  }
  return solucion;

}


/*
****************DIFERENCIA ***********
*/

set<int> diferencia(set<int> c1, set<int> c2){
  set<int> solucion;
  set<int>::iterator i;

  for(i=c1.begin();i!=c1.end();++i){
    if(!c2.count(*i)){//Comprueba si no está p está en c2
      solucion.insert(*i);//inserto en solucion
    }
  }
  return solucion;
}


//Comprueba que el numero de pares
//en el conjunto es mayor que el de impares
bool masparesqueimpares(const set<int> & c, set<int> & cpar, set<int> & cimpar){
  set<int>::iterator p;
  for(p=c.begin();p!=c.end();++p){//Recorro el conjunto
    if((*p)%2==0){
      cpar.insert(*p);
    }
    else{
      cimpar.insert(*p);
    }
  }
  return cpar.size() > cimpar.size();

}

bool masdelamitadcomunes(const set<int> & c1, const set<int> & c2){

  set<int>::const_iterator p;
  int i,j;
  i=0;
  j=c1.size();

  for(p=c1.begin();p!=c1.end();++p){//Recorro y compruebo que p está en c2
    if(c2.count(*p)){
      i++;//Voy avanzando
    }
  }
  return (i>j/2);//Compruebo si los elementos comprobados son mayores que el total

}

/*
Construir una funcion para determinar
si un conjunto tiene todos los elementos
pares incluidos dentro de otro
*/

bool inclusionpares(const set<int> & c1, const set<int> & c2){

  set<int>::iterator p;
  int i,j;
  i=0;
  j=c1.size();

  for(p=c1.begin();p!=c1.end();++p){//Recorro y compruebo que p está en c2
    if((*p)%2==0){
      if(c2.find(*p)==c2.end()){
        return false;
      }
      else{
        ++p;
      }
    }
}
return true;
}

bool sonDisjuntos(const set<int> &s1, const set<int> &s2){
      set<int> s3;
      interseccion(s1, s2, s3);
      return s3.size() == 0;

}

/**
    * @brief Devuelve si un elemento está en la intersección de otros dos
    * @param s1 Primer conjunto
    * @param s2 Segundo conjunto
    * @return true si el elemento está en ambos conjuntos, false en caso contrario.
    */
    template <class T>
    bool estaEnLaInterseccion(const set <int> &s1, const set <int> &s2, T elemento){
      set <int> s3;
      interseccion(s1, s2, s3);
      if(s3.count(elemento))
       return true;

     else
       return false;
    }


  void interseccionATres(const set<int>& s1, const set<int>& s2, const set<int>& s3, set<int>& result) {

   set<int>::iterator it;
  for (it = s1.begin(); it != s1.end(); it++) {
    if (s2.count(*it)  && s3.count(*it) ) {
      result.insert(*it);
    }
  }
}

/*
Añade a result los elementos de s1 que no están ni en s2 ni en s3
*/
void unionDiferencialParcial(const set<int>& s1, const set<int>& s2, const set<int>& s3, set<int>& result) {
  for (set<int>::iterator it = s1.begin(); it != s1.end(); it++) {
    if (s2.count(*it) == 0 && s3.count(*it) == 0) {
      result.insert(*it);
    }
  }
}

/*
    Podríamos calcular la unión de los tres conjuntos y quitarle la intersección
    dos a dos: AUBUC \ (intersec(A,B) U intersec(A,C) U intersec(B,C))
    Esto sería tremendamente más costoso ya que para conjuntos grandes con muchos
    elementos en la interseccion de los 3 estaríamos copiando los mismos valores varias veces.
    Manteniendo la misma notación, la eficiencia de esta función es de:
      S1 * (log (S2) + log (S3)) +
      S2 * (log (S1) + log (S3)) +
      S3 * (log (S1) + log (S2))
    Esto es lo mismo, en notación O(n), al máximo valor de los tres sumandos
*/

void unionDiferencial(const set<int>& s1, const set<int>& s2, const set<int>& s3, set<int>& result) {
  result.clear();
  unionDiferencialParcial(s1, s2, s3, result);
  unionDiferencialParcial(s2, s1, s3, result);
  unionDiferencialParcial(s3, s1, s2, result);
}


// Unión diferencial aplicada a un conjunto de conjuntos

void superUnionDiferencial(const set< set<int> >& S, set<int>& result) {
  result.clear();
   set< set<int> >::iterator setIt, setIt2;
   set<int>::iterator it;
  bool incluir;
  for (setIt = S.begin(); setIt != S.end(); setIt++) {
    for (it = (*setIt).begin(); it != (*setIt).end(); it++) {
      incluir = true;
      setIt2 = S.begin();
      while (incluir && setIt2 != S.end()) {
        if (setIt != setIt2) {
          incluir = (*setIt2).count(*it) == 0;
        }
        setIt2++;
      }
      if (incluir) {
        result.insert(*it);
      }
    }
  }
}


set <int> operator-(const set<int>& A, const set<int>& B){
  set <int> C;
   set<int>::iterator i;

  for (i = A.begin(); i != A.end(); ++i){
    if (B.count(*i) == 0)
      C.insert(*i);
  }

  return C;

}


/*****************************************************************************/

/**
    @brief Obtiene la unión de dos conjuntos de enteros.
*/
set<int> obtenerUnion(const set<int> &uno, const set<int> &otro) {
    set<int> resultado;
    set<int>::const_iterator it;
    //Inserto el primer set.
    for (it = uno.begin(); it != uno.end() ; ++it)
        resultado.insert(*it);
    //Inserto el segundo set.
    for (it = otro.begin() ; it != otro.end() ; ++it)
        resultado.insert(*it);
    return resultado;
}

/**
    @brief Obtiene la diferencia simétrica de dos conjuntos de enteros.
*/
set<int> obtenerDiferenciaSimetrica(const set<int> &uno, const set<int> &otro) {
    set<int>::const_iterator it;
    set<int> resultado;
    for (it = uno.begin() ; it != uno.end() ; ++it)
        if (otro.count(*it) == 0)                       //Si está sólo en el primero.//Si no está en otro
            resultado.insert(*it);

    for (it = otro.begin(); it != otro.end() ; ++it)
        if (uno.count(*it) == 0)                        //Si está solo en el segundo.//Si no está en uno
            resultado.insert(*it);

    return resultado;
}

/**
    @brief Función que calcula los n elementos mayores de dos conjuntos.
    @param n Número de máximos a calcular.
    NOTA: si pasamos un n mayor que el tamaño de la unión de los conjuntos, optaré por devolver la propia unión, ya que ésta
          contiene los ( union.size() ) mayores elementos del conjunto.
          Podría también terminar el programa mediante un assert( n <= laUnion.size() ), pero me parece una forma un tanto
          "violenta" de finalizar el programa.
*/
set<int> obtenerMayores (int n, const set<int> &uno, const set<int> &otro) {
    //Calculamos la unión para trabajar sobre un solo conjunto y poder modificarlo.
    set<int> laUnion = obtenerUnion(uno, otro);
    set<int> resultado;
    set<int>::const_iterator it;
    //Si n es mayor o igual que el tamaño de la unión, los máximos coinciden con la unión.
    if (n >= laUnion.size())
        resultado = laUnion;
    else {
        for (int i = 0; i < n; i++) {                                   //Lo hacemos n veces
            int max = INT_MIN;                                          //Reseteamos la variable cada vez que obtenemos un máximo.
            for (it = laUnion.begin() ; it != laUnion.end() ; ++it) {   //Bucle que calcula el máximo.
                if (*it > max)
                    max = *it;
            }
            resultado.insert(max);                                      //Insertamos en el resultado.
            laUnion.erase(max);                                         //Eliminamos de la unión.
        }
    }
    return resultado;
}
int main(){
  set<int> s1, s2, s3,result;

  for(int i= 0; i< 5; i++){
    s1.insert(GeneraEntero(1,50));
    s2.insert(GeneraEntero(1,50));
    s3.insert(GeneraEntero(1,50));
  }

  cout << "Conjunto 1: " << s1 << "\nConjunto 2: " << s2 << "\nConjunto 3: " << s3;
  //Probamos que funciona la unión de conjuntos
  set<int> u;
  _union(s1, s3, u);
  cout << "\nConjunto resultante de unir el conjunto 1 y el 3: " << u;
  //Probamos que funciona la intersección de conjuntos
  set<int> s4;
  interseccion(s1, s2, s4);
  cout << "\nConjunto resultante  de la interseccion el conjunto 1 y 2(elementos comunes): " << s4;

  cout<<endl;

  if(sonDisjuntos(s1, s2))
    cout << "El conjunto 1 y el 2 son disjuntos";
  else{
    set<int> s5;
    interseccion(s1, s2, s5);
    cout << "\nLos conjuntos 1 y 2 tienen los elementos " << s5 << " en común";
  }
  cout<<endl;
  set <int> s6= (s1-s2), s7= (s2-s1);
  cout << "S6 es: "<<s6<<endl;
  cout << "S7 es: "<<s7<<endl;
  cout<<endl;




  unionDiferencial(s1, s2, s3, result);
  cout << "Unión diferencial de los tres = " << result << endl << endl;

  set< set<int> > S;
  S.insert(s1);
  S.insert(s2);
  S.insert(s3);
  superUnionDiferencial(S, result);
  cout << "Super unión diferencial de los tres = " << result << endl << endl;



      set<int> laUnion;
      laUnion = obtenerUnion(s1, s2);
      cout << "La union de S1 y S2 es " << laUnion << endl;

      set<int> laDiferenciaSimetrica;
      laDiferenciaSimetrica = obtenerDiferenciaSimetrica(s1, s2);
      cout << "La diferencia simétrica de S1 y S2 es " << laDiferenciaSimetrica << endl;

      set<int> maximos = obtenerMayores(5, s1, s2);
      cout << "Para n = 5, el conjunto de máximos de S1 y S2 es " << maximos << endl;





      set<int> c3,c4;

      for(int i =0 ;i<11;i++){
        c3.insert(i);
      }
      for(int j =0 ;j<10;j++){
        c4.insert(j);
      }

      interseccionATres(s1, s2, s3, result);
      cout << "Intersección de los tres = " << result << endl << endl;

      set<int> c1,c2,conjuntofinal;

      for(int i =0 ;i<17;i++){
        conjuntofinal.insert(i);
      }
      cout<<"¿Hay más pares que impares? "<<masparesqueimpares(c1,c2,conjuntofinal)<<endl;
      cout<<"¿Hay más de la mitad comunes? "<<masdelamitadcomunes(c3,c4)<<endl;
      cout<<"¿Están incluidos los pares? "<<inclusionpares(c3,c4)<<endl;

}
