/**
  * @file fechahistorica.cpp
  * @brief Fichero implementacion del TDA FechaHistorica
  * @author Sofía Fernández Moreno
  */
#include "fechahistorica.h"

using namespace std;

/******************************************************************************/

FechaHistorica::FechaHistorica(){
    cabecera = 0;
}

/******************************************************************************/

FechaHistorica::FechaHistorica(string cadena){
    insertar(cadena);
}

/******************************************************************************/

FechaHistorica::FechaHistorica(const FechaHistorica &otra){
    copiarLista(otra);
}

/******************************************************************************/

FechaHistorica::~FechaHistorica(){
  destruir();
}

/******************************************************************************/

void FechaHistorica::destruir(){
  EventoHistorico *ptr = cabecera;
  columnas = 0;
  fecha = 0;
  while (cabecera != 0) {
    ptr=cabecera;
    cabecera=cabecera->siguiente;
    delete ptr;
  }
  delete cabecera;
}

/******************************************************************************/

FechaHistorica &FechaHistorica::copiarLista(const FechaHistorica &otra){
  if(otra.cabecera != 0){
    destruir();
    EventoHistorico *aux = otra.cabecera;

    this->fecha = otra.fecha;
    this->columnas = otra.columnas;

    while(aux->siguiente != 0){
      insertar(aux->evento);
      aux = aux -> siguiente;
    }

    delete aux;
  }

  return *this;
}

/******************************************************************************/

FechaHistorica &FechaHistorica::operator=(const FechaHistorica &otra){
  if(otra.cabecera != 0){
    destruir();
    copiarLista(otra);
  }

  return *this;
}

/******************************************************************************/

void FechaHistorica::insertar(string event){

  EventoHistorico *evento = new EventoHistorico();

  evento -> evento = event;
  evento -> siguiente = 0;

  if(cabecera== 0)
    cabecera = evento;
  else{
    EventoHistorico *ptr;
    ptr = cabecera;
    while (ptr->siguiente != 0) ptr = ptr->siguiente;
    ptr->siguiente = evento;
  }

  this->columnas++;
}

/******************************************************************************/

void FechaHistorica::insertarLista(string list){

  string evento;
  evento.resize(list.size());
  int anio = stoi(list.substr(0,4));
  this->fecha = anio;

  int cnt = 0;
  for (int i = 5; i < (int)list.size(); i++) {
    if(list[i] != '#'){
      evento.push_back(list[i]);
      cnt++;
    }else{
      insertar(evento);
      evento.clear();
      cnt = 0;
    }
  }

  // En caso de no insertar el último lo inserto
  insertar(evento);
}

/******************************************************************************/

string FechaHistorica::getEvento(int i){
  string cadena = "";
  EventoHistorico *ptr = cabecera;
  int cnt = 0;
  if(ptr != 0){
    while (cnt != i) {
      ptr = ptr -> siguiente;
      cnt++;
    }
    cadena = ptr-> evento;
  }
  return cadena;
}

/******************************************************************************/

vector<string> FechaHistorica::getEventos(){
  std::vector<string> v;
  string evento;
  for (int i = 0; i < this->columnas; i++) {
    evento = getEvento(i);
    v.push_back(evento);
  }
  return v;
}

/******************************************************************************/

int FechaHistorica::getColumnas(){
  return this->columnas;
}

/******************************************************************************/

const int FechaHistorica::getFecha(){
  return this->fecha;
}

/******************************************************************************/
