#include "cronologia.h"

#include <fstream>
#include <iostream>

using namespace std;

int main(int argc, char * argv[]){
  Cronologia cronologia;
  if (argc!=2){
      cout<<"Dime el nombre del fichero con la cronologia"<<endl;
      return 0;
   }

   ifstream f (argv[1]);
   if (!f){
    cout<<"No puedo abrir el fichero "<<argv[1]<<endl;
    return 0;
   }

  
   f>>cronologia; 

   int anio;
   cout<<"Introduzca el año a consultar: ";
   cin >> anio;

   vector<string> eventos = cronologia.GetEventos(anio);

   /* Escribimos */
   cout << anio << ": "<< endl;
  for (int i=0;i<(int)eventos.size(); i++)
    cout << "  **** [ " << eventos[i] << " ] ****"<<endl;

}
