/**
  * @file Pila_max_Cola.h
  * @brief Fichero cabecera del TDA Cola
  *
  * Gestiona una secuencia de elementos con facilidades para la inserción y
  * borrado de elementos en un extremo
  */

  #ifndef __Cola_H__
  #define __Cola_H__
  
  #include <cassert>
  
  /**
   *  @brief T.D.A. PilaMax basado en Colas
  
   * @author Sofia Fernandez Moreno
   * @date Noviembre 2017
  */
  template <class T>
  class Pilamax {
  
    private:
      struct Celda {
        T elemento;
        T max;
        Celda * siguiente; ///< Puntero al siguiente nodo.
  
        /**
         * @brief Constructor por defecto
         */
        Celda() : siguiente(0){
        }
        /**
         * @brief Constructor con parámetros
         * @param elem Elemento de información.
         * @param sig Puntero al siguiente nodo.
         */
        Celda(const T & elem, Celda * sig): elemento(elem), siguiente(sig){
        }
      };
  
      Celda * primera; ///< Puntero al primer nodo de la lista.
      Celda * ultima;  ///< Puntero al último nodo de la lista.
      int num_elem;    ///< Número de elementos de la cola.
  
    public:
      // ---------------  Constructores ----------------
      /**
       * @brief Constructor por defecto
       */
      Pilamax(): primera(0), ultima(0), num_elem(0){
      }
  
      /**
       * @brief Constructor de copias
       * @param original La cola de la que se hará la copia.
       */
      Pilamax(const Pilamax<T> & original);
      // ------------------ Destructor ------------------
      /**
       * @brief Destructor
       */
      ~Pilamax();
  
      // --------------- Otras funciones ---------------
  
      /**
       * @brief Operador de asignación
       * @param otra La cola que se va a asignar.
       */
      Pilamax& operator= (const Pilamax<T> & otra);
  
      /**
       * @brief Comprueba si la cola está vacía
       */
      bool Empty() const{
        return num_elem == 0;
      }
  
      /**
       * @brief Devuelve el elemento del frente de la cola
       */
      T Top() const{
        assert(primera != 0);        //Si la cola está vacía, abortar
        return this->primera->max;
      }
  
      /**
       * @brief Devuelve el elemento del frente de una cola constante
       */
      T Max() const{
        assert(primera != 0);        //Si la cola está vacía, abortar
        return this->primera->max;
      }
  
  
      /**
       * @brief Añade un elemento al final de la cola
       * @param elem Elemento que se va a añadir.
       */
      void Push(const T & elem);
  
      /**
       * @brief Quita el elemento del frente de la cola
       */
      void Pop();
  
      /**
       * @brief Devuelve el número de elementos de la cola
       */
      int Size() const{
        return num_elem;
      }
  
  
  
      void Swap(Pilamax &otra);
  };
  
  #include "Pila_max_Cola.cpp"
  
  #endif // __Cola_H__
  