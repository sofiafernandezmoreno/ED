#include <iostream>
#include "Pila_max_Cola.h"

using namespace std;

int main() {
  Pilamax<int> p;

  for (int i=0; i<10 ; ++i){
    p.Push(i);
  }

  while (!p.Empty() ){
    cout << "Valor: " << p.Top() << "\tMáximo: " << p.Max() << endl;
    p.Pop();
  }

  return 0;
}
