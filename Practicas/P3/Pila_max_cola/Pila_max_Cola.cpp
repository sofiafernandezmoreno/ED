
  #include <cassert>
  
  /* _________________________________________________________________________ */
  
  
  template <class T>
  Pilamax<T>::Pilamax(const Pilamax<T> & original){
    if (original.primera!=0){           //Si tiene elementos
      Celda *p = original.primera;      //Copiamos el puntero a la primera celda
      ultima = new Celda(p->elemento,0); //Inicializamos la lista de nodos
      p = p->siguiente;                     //Avanzamos el puntero
      while (p!=0){                         //Mientras queden elementos
        ultima->siguiente = new Celda(p->elemento,0); //Copiamos el elemento
        ultima = ultima->siguiente;                   //Avanzamos los punteros
        p = p->siguiente;
      }
    }
    else
      primera = ultima = 0;            //Si no tiene elementos
    num_elem = original.num_elem;
  }
  
  /* _________________________________________________________________________ */
  
  template <class T>
  Pilamax<T>::~Pilamax(){
    Celda * aux;
    while (primera!=0){             //Mientras queden elementos
      aux = primera;                //Copiamos el puntero
      primera = primera->siguiente; //Avanzamos primera
      delete aux;                   //Borramos el nodo
    }
  }
  
  /* _________________________________________________________________________ */
  
  template <class T>
  Pilamax<T>& Pilamax<T>::operator=(const Pilamax<T> & otra){
    Celda * p;
  
    if (this != &otra){    //Comprobación de rigor. Si son diferentes objetos
      while (primera!=0){  //Borramos la lista de nodos de la cola *this
        p = primera;
        primera = primera->siguiente;
        delete p;
      }
      if (otra.primera!=0){        //Si la otra cola tiene elementos
        p = otra.primera;          //Copiamos el puntero al primer nodo
        primera=ultima= new Celda(p->elemento,0); //Reservamos el primer nodo
        p=p->siguiente;                           //Avanzamos el puntero
        while (p!=0){                             //Mientras queden elementos
          ultima->siguiente=new Celda(p->elemento,0); //Creamos un nuevo nodo
          ultima=ultima->siguiente;                   //Actualizamos ultima
          p=p->siguiente;                             //Avanzamos el puntero
        }
      }
      else primera=ultima=0;      //Si la otra cola está vacía
      num_elem=otra.num_elem;
    }
    return *this; //Devolvemos el objeto para permitir el encadenamiento (a=b=c)
  }
  
  /*_________________________________________________________________________ */
  
  template <class T>
  void Pilamax<T>::Push(const T & elem){
    T maxAux = elem;
    Celda *aux = new Celda();    //Creamos un nuevo nodo
  
    if (primera==0) {                   //Si la lista está vacía,
      this->primera = ultima = aux;          //primera y ultima apuntan a ese nodo
      this->ultima->elemento = this->ultima->max = elem;
    }
    else{                      //Si la lista ya tenia nodos,
  
      if (this->primera->max >= elem) {
        maxAux = this->primera->max;
      }
      aux->siguiente = this->primera;
      this->primera = aux;            //Actualizamos ultima
      this->primera->elemento = elem;
      this->primera->max = maxAux;
    }
  
    num_elem++;                //Incrementamos el numero de elementos
  }
  
  /* _________________________________________________________________________ */
  template <class T>
  void Pilamax<T>::Pop() {
    assert(primera!=0);             //Si la cola está vacía, abortar
    Celda *aux = primera;           //Copiamos el puntero al primer nodo
    primera = primera->siguiente;   //Actualizamos primera
    delete aux;                     //Borramos el primer nodo
    if (primera == 0)               //Si no quedan nodos,
      ultima=0;                     //actualizamos ultima
    num_elem--;                     //Actualizamos el número de elementos
  }
  
  template<class T>
  void Pilamax<T>::Swap(Pilamax &otra) {
    Pilamax aux(otra);
  
    otra->primera = this->primera;
    otra->ultima = this->ultima;
    otra->num_elem = this->num_elem;
  
    this->primera = aux->primera;
    this->ultima = aux->ultima;
    this->num_elem = aux->num_elem;
  }
  