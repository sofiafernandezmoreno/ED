#include <iostream>
using namespace std;

/**
   Constructor por defecto de la pila.
*/
template <class T>
Pilamax<T>::Pilamax() {
   this->vecDatos = new T[size];
   this->vecMax = new T[size];
   this->size = 0;
   this->ult = 0;
}

/**
   Constructor de copia de la pila.
*/
template <class T>
Pilamax<T>::Pilamax(const Pilamax &otra) {
   this->vecDatos = otra.vecDatos;
   this->vecMax = otra.vecMax;
   this->size = otra.size;
}

/**
   Destructor de la clase.
*/
template <class T>
Pilamax<T>::~Pilamax(){
   delete [] this->vecDatos;
   delete [] this->vecMax;
}

/**
   Introduce un valor al final de la pila.
   Es el unico lugar donde se pueden intro-
   ducir elementos en la pila.
*/
template <class T>
void Pilamax<T>::Push(T dato) {
   T *newdatos;
   T *newmaximos;

   if (this->size == 0) {
      newdatos = new T[5];
      newmaximos = new T[5];
      this->size = 5;
   }

   if (this->ult == this->size) {
      newdatos = new T[2 * this->size];
      newmaximos = new T[2 * this->size];
      this->size = 2 * this->size;
   }

   for (int i = 0; i <= this->ult; ++i) {
      newdatos[i] = this->vecDatos[i];
      newmaximos[i] = this->vecMax[i];
   }

   this->vecDatos = newdatos;
   this->vecMax = newmaximos;

   delete[] newdatos;
   delete[] newmaximos;

   this->vecDatos[ult] = dato;

   if (dato > this->vecMax[ult] || this->ult == 0) {
      this->vecMax[ult] = dato;
   } else {
      this->vecMax[ult] = this->vecMax[ult-1];
   }

   this->ult++;
}

/**
   Elimina la ultima posicion de la pila.
   Unica posición que podemos eliminar.
*/
template <class T>
void Pilamax<T>::Pop() {
/* T *auxDat;
   T *auxMax;


   if (this->ult == this->size/4) {
      delete [] this->vecDatos;
      delete [] this->vecMax;
      this->size /= 4;
      auxDat = new T[this->size];
      auxMax = new T[this->size];

      for (int i = 0; i < this->ult; ++i) {
         auxDat[i] = this->vecDatos[i];
         auxMax[i] = this->vecMax[i];
      }

      this->vecDatos = auxDat;
      this->vecMax = auxMax;
      delete [] auxDat;
      delete [] auxMax;
   }*/

   this->ult--;
}

/**
   Comprueba si la pila esta vacia.
   True:   Vacia
   False:  Tiene al menos un elemento.
*/
template <class T>
bool Pilamax<T>::Empty() {
   bool vacia = false;

   if (this->ult == 0) {
      vacia = true;
   }

   return vacia;
}

/**
   Devuelve el tamaño de la pila.
*/
template <class T>
int Pilamax<T>::Size() {
   return this->size;
}

/**
   Devuelve el último elemento de la pila.
   Es el unico dato que podemos obtener de
   la pila. El resto de datos almacenados
   en la pila no son accesibles.
*/
template <class T>
T Pilamax<T>::Top() {
   return this->vecDatos[ult-1];
}

/**
 * Devuelve le máximo de la pila.
*/
template <class T>
T Pilamax<T>::Max() {
   return this->vecMax[ult-1];
}

template <class T>
void Pilamax<T>::Swap(Pilamax &otra) {
   Pilamax aux(otra);

   otra->size = this->size;
   otra->ult = this->ult;
   otra->vecDatos = this->vecDatos;
   otra->vecMax = this->vecMax;

   this->size = aux->size;
   this->ult = aux->ult;
   this->vecDatos = aux->vecDatos;
   this->vecMax = aux->vecMax;
}
