/**
  * @file Pila_max_VD.h
  * @brief Fichero cabecera del TDA Pila con Vector dinámico.
  */

  #ifndef __Vector_H__
  #define __Vector_H__
  
     /**
       * @brief T.D.A. Pilamax (implementada con vectores dinámicos).
       *
       * Ejemplo de uso:
       * @include usopilas_max.cpp
       *
       * @author Ángel Gómez Martín, Salvador Corts Sánchez
       * @date Noviembre 2016
      */
  
  template <class T>
  class Pilamax{
  private:
      /**
        * @page Conjunto Rep del TDA Pila (a partir de una lista con celdas enlazadas).
        *
        * @section Función de abstracción
        *
        * Un objeto válido del TDA Pilamax representa:
        *
        * Una pila implementada con vectores dinámicos que nos permite almacenar una
        * serie de valores así como el máximo de ellos en cada momento.
        * 
       */
    
      /**
       * @brief Elementos privados del TDA Pilamax.
       * @param *vecDatos Puntero a vector dinámico que guarda los datos de la pila.
       * @param *vecMax Puntero a vector dinámico que guarda los valores máximos de la pila.
       * @param size Tamaño actual de la pila.
       * @param ult Ultima posición ocupada en la pila.
      */
     T *vecDatos;
     T *vecMax;
     int size;
     int ult;
  
  public:
     /* Funciones que componen el TDA.
     
         *    Constructores   Constructores de la pila.
         *    Empty           Comprueba si la pila esta vacia.
         *    Size            Devuelve el tamaño.
         *    Top             Accede al ultimo elemento.
         *    Push            Inserta un elemento.
         *    Pop             Borra el ultimo elemento.
         *    Swap            Intercambia dos elementos.
     */
  
     /**
      * @brief Constructor por defecto de la pila.
     */
     Pilamax();
  
     /**
      * @brief Constructor de copia de la pila.
      * @param otra Pila a copiar.
     */
     Pilamax(const Pilamax &otra);
     
     /**
      * @brief Destructor de la pila.
     */
     ~Pilamax();
  
     /**
      * @brief Introduce un valor al final de la pila.
      * @param dato Dato a introducir en la pila.
      * @doc Esta función introduce un valor al final de la pila, el unico lugar
      * donde se pueden introducir elementos.
     */
     void Push(T dato);
  
     /**
      * @brief Elimina la ultima posicion de la pila.
      * @doc Unica posición que podemos eliminar.
     */
     void Pop();
  
     /**
      * @brief Comprueba si la pila esta vacia.
      * @doc True:   Vacia
      * @doc False:  Tiene al menos un elemento.
     */
     bool Empty();
  
     /**
      * @brief Devuelve el tamaño de la pila.
     */
     int Size();
  
     /**
      * @brief Devuelve el último elemento de la pila.
      * @doc Es el unico dato que podemos obtener de
      * la pila. El resto de datos almacenados en la
      * pila no son accesibles.
     */
     T Top();
     
     /**
      * @brief Devuelve el elemento máximo de la pila.
     */
     T Max();
     
     /**
      * @brief Intercambia los valores de dos pilas.
      * @param pila_2 Pila que se intercambia con la que invocamos la función.
      * @doc Esta función intercambia dos pilas completamente. Todos los valores
      * que corresponden a cada pila quedan permutados.
     */
     void Swap(Pilamax &otra);
  
  };
  
  #include "Pila_max_VD.cpp"
  #endif
  