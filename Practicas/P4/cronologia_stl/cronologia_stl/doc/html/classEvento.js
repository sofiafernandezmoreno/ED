var classEvento =
[
    [ "Evento", "classEvento.html#a0e5e513e4d3461ed93753b05e378ae0c", null ],
    [ "Evento", "classEvento.html#ac2c66c3478aeafcbb6c9e22ccca953c4", null ],
    [ "~Evento", "classEvento.html#a1fe5f7549736a17562670d0ac3941bec", null ],
    [ "AniadeEvento", "classEvento.html#a788fe9e92c438b724ed12ee156285533", null ],
    [ "BuscaPalabra", "classEvento.html#af328c0e845dc77d8150ffcf170878f17", null ],
    [ "GetEventos", "classEvento.html#aaacee38ce29d83e1bc10bf4bfd7a547c", null ],
    [ "GetVecEventos", "classEvento.html#a18365c2bf9587db462718d55ff7ca1ba", null ],
    [ "GetVecPalabras", "classEvento.html#a768235a06f9446e4051d3821ff504c20", null ],
    [ "PalabraEnRango", "classEvento.html#a9b9f5b04d8b143683bf816d8ef6b903d", null ],
    [ "Separar", "classEvento.html#ac21440c7edd9d182de0cc637bd641db2", null ],
    [ "SetAnio", "classEvento.html#ae0a6d66d97563830dbc892ce77841d2f", null ],
    [ "anio", "classEvento.html#a4d12d37ec59f7057e996cc27d7591cae", null ],
    [ "blackListed", "classEvento.html#a1400ae6ca09e115d64323dc903c9f0ad", null ],
    [ "vecEventos", "classEvento.html#a75bc40c5154cbb35b6425ef7f077c735", null ],
    [ "vecPalabras", "classEvento.html#af2712591dacc8d8991ecb1ab2b88275b", null ]
];