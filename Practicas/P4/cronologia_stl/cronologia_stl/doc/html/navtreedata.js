var NAVTREE =
[
  [ "Cronología", "index.html", [
    [ "Rep del TDA Cronología.", "repConjunto.html", [
      [ "Invariante de la representación", "repConjunto.html#invConjunto", null ],
      [ "Función de abstracción", "repConjunto.html#faConjunto", null ]
    ] ],
    [ "Clases", "annotated.html", [
      [ "Lista de clases", "annotated.html", "annotated_dup" ],
      [ "Índice de clases", "classes.html", null ],
      [ "Miembros de las clases", "functions.html", [
        [ "Todo", "functions.html", null ],
        [ "Funciones", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Archivos", null, [
      [ "Lista de archivos", "files.html", "files" ],
      [ "Miembros de los ficheros", "globals.html", [
        [ "Todo", "globals.html", null ],
        [ "Funciones", "globals_func.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"TDACronologia_8h.html"
];

var SYNCONMSG = 'click en deshabilitar sincronización';
var SYNCOFFMSG = 'click en habilitar sincronización';