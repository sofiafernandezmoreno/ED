\select@language {spanish}
\contentsline {chapter}{\numberline {1}Rep del T\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}DA Cronolog\IeC {\'\i }a.}{1}
\contentsline {section}{\numberline {1.1}Invariante de la representaci\IeC {\'o}n}{1}
\contentsline {section}{\numberline {1.2}Funci\IeC {\'o}n de abstracci\IeC {\'o}n}{1}
\contentsline {section}{\numberline {1.3}Invariante de la representaci\IeC {\'o}n}{1}
\contentsline {section}{\numberline {1.4}Funci\IeC {\'o}n de abstracci\IeC {\'o}n}{1}
\contentsline {chapter}{\numberline {2}\IeC {\'I}ndice de clases}{3}
\contentsline {section}{\numberline {2.1}Lista de clases}{3}
\contentsline {chapter}{\numberline {3}Indice de archivos}{5}
\contentsline {section}{\numberline {3.1}Lista de archivos}{5}
\contentsline {chapter}{\numberline {4}Documentaci\IeC {\'o}n de las clases}{7}
\contentsline {section}{\numberline {4.1}Referencia de la Clase Cronologia}{7}
\contentsline {subsection}{\numberline {4.1.1}Descripci\IeC {\'o}n detallada}{8}
\contentsline {subsection}{\numberline {4.1.2}Documentaci\IeC {\'o}n del constructor y destructor}{8}
\contentsline {subsubsection}{\numberline {4.1.2.1}Cronologia(const Cronologia \&c)}{8}
\contentsline {subsection}{\numberline {4.1.3}Documentaci\IeC {\'o}n de las funciones miembro}{9}
\contentsline {subsubsection}{\numberline {4.1.3.1}Aniade\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Anio(const int anio, const Evento eventos)}{9}
\contentsline {subsubsection}{\numberline {4.1.3.2}Busca\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Anio(const int anio)}{9}
\contentsline {subsubsection}{\numberline {4.1.3.3}Buscar(const string palabra)}{9}
\contentsline {subsubsection}{\numberline {4.1.3.4}Get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Eventos(const int anio)}{9}
\contentsline {subsubsection}{\numberline {4.1.3.5}Get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Vec\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Anios() const }{9}
\contentsline {subsubsection}{\numberline {4.1.3.6}Get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Vec\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Eventos() const }{10}
\contentsline {subsubsection}{\numberline {4.1.3.7}Tamanio() const }{10}
\contentsline {subsection}{\numberline {4.1.4}Documentaci\IeC {\'o}n de los datos miembro}{10}
\contentsline {subsubsection}{\numberline {4.1.4.1}vec\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Anios}{10}
\contentsline {subsubsection}{\numberline {4.1.4.2}vec\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Eventos}{10}
\contentsline {section}{\numberline {4.2}Referencia de la Clase Evento}{10}
\contentsline {subsection}{\numberline {4.2.1}Descripci\IeC {\'o}n detallada}{11}
\contentsline {subsection}{\numberline {4.2.2}Documentaci\IeC {\'o}n del constructor y destructor}{12}
\contentsline {subsubsection}{\numberline {4.2.2.1}Evento(const int anio)}{12}
\contentsline {subsubsection}{\numberline {4.2.2.2}Evento(const Evento \&c)}{12}
\contentsline {subsection}{\numberline {4.2.3}Documentaci\IeC {\'o}n de las funciones miembro}{13}
\contentsline {subsubsection}{\numberline {4.2.3.1}Aniade\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Evento(const string evento)}{13}
\contentsline {subsubsection}{\numberline {4.2.3.2}Busca\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Palabra(const string palabra, Evento \&eventos)}{13}
\contentsline {subsubsection}{\numberline {4.2.3.3}Get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Eventos()}{13}
\contentsline {subsubsection}{\numberline {4.2.3.4}Get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Vec\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Eventos() const }{13}
\contentsline {subsubsection}{\numberline {4.2.3.5}Get\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Vec\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Palabras() const }{14}
\contentsline {subsubsection}{\numberline {4.2.3.6}Palabra\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}En\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Rango(const string palabra)}{14}
\contentsline {subsubsection}{\numberline {4.2.3.7}Set\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Anio(const int anio)}{14}
\contentsline {subsection}{\numberline {4.2.4}Documentaci\IeC {\'o}n de los datos miembro}{14}
\contentsline {subsubsection}{\numberline {4.2.4.1}anio}{14}
\contentsline {subsubsection}{\numberline {4.2.4.2}black\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Listed}{14}
\contentsline {subsubsection}{\numberline {4.2.4.3}vec\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Eventos}{14}
\contentsline {subsubsection}{\numberline {4.2.4.4}vec\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Palabras}{14}
\contentsline {chapter}{\numberline {5}Documentaci\IeC {\'o}n de archivos}{15}
\contentsline {section}{\numberline {5.1}Referencia del Archivo include/\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}T\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}D\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}A\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Cronologia.h}{15}
\contentsline {subsection}{\numberline {5.1.1}Descripci\IeC {\'o}n detallada}{15}
\contentsline {subsection}{\numberline {5.1.2}Documentaci\IeC {\'o}n de las funciones}{15}
\contentsline {subsubsection}{\numberline {5.1.2.1}operator$<$$<$(ostream \&os, const Cronologia \&cronologia)}{15}
\contentsline {subsubsection}{\numberline {5.1.2.2}operator$>$$>$(istream \&is, Cronologia \&cronologia)}{16}
\contentsline {section}{\numberline {5.2}Referencia del Archivo include/\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}T\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}D\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}A\discretionary {\unhbox \voidb@x \hbox {\relax \fontsize {7}{8}\selectfont $\leftarrow \joinrel \rhook $}}{}{}Evento.h}{16}
\contentsline {subsection}{\numberline {5.2.1}Descripci\IeC {\'o}n detallada}{17}
\contentsline {subsection}{\numberline {5.2.2}Documentaci\IeC {\'o}n de las funciones}{17}
\contentsline {subsubsection}{\numberline {5.2.2.1}operator$<$$<$(ostream \&os, const Evento \&evento)}{17}
\contentsline {chapter}{\IeC {\'I}ndice}{19}
