#include <iostream>
#include <string>
#include <map>
#include <ostream>
#include <istream>
#include <utility>	
#include "Cronologia.h"
#include "EventoHistorico.h"

using namespace std;

Cronologia::Cronologia() {
}

Cronologia::Cronologia(const Cronologia& otra) {
	(*this).mapa = otra.mapa;
}

Cronologia::~Cronologia() {
	(*this).mapa.clear();
}

Cronologia::iterator Cronologia::begin() {
	return (*this).mapa.begin();
}

Cronologia::const_iterator Cronologia::begin() const {
	return (*this).mapa.cbegin();
}

Cronologia::iterator Cronologia::end() {
	return (*this).mapa.end();
}

Cronologia::const_iterator Cronologia::end() const {
	return (*this).mapa.cend();
}

int Cronologia::size() {
	return (*this).mapa.size();
}

bool Cronologia::empty() {
	return (*this).mapa.empty();
}

EventoHistorico Cronologia::at(int year) {
	return (*this).mapa.at(year);
}

void Cronologia::emplace(int anio, EventoHistorico evento) {
	(*this).mapa.emplace(anio, evento);
}

void Cronologia::erase(int anio) {
	iterator it = (*this).find(anio);
	(*this).mapa.erase(it);
}

Cronologia::iterator Cronologia::find(int year) {
	return (*this).mapa.find(year);
}

EventoHistorico Cronologia::GetEventos(int year) {
	iterator it = (*this).find(year);	// Busca la key year y devuelve su iterador

	return (*it).second;	// Devuelve el dato con la key year
}

// Sobrecarga del operador >>
istream& operator>>(istream& is, Cronologia &cronologia) {
	string dato;
	int anio;
	EventoHistorico* evento;
 
	while (!is.eof()) {
	   is >> anio;
			 evento = new EventoHistorico(anio);	// Crea un evento con ese año
	   is.ignore();   // Ignoramos el #
 
			 dato = "";
	   while (is.peek() != '\n' && is) {
				  dato = "";
		  while (is.peek() != '#' && is.peek() != '\r' && is) {
			 dato += is.get();
		  }
		  is.ignore();   // Ignoramos el #
				  evento->AniadeEventoHistorico(dato);	// Añade sucesos a ese objeto Evento
	   }
	   is.ignore();   // Ignoramos el \n
 
			 cronologia.emplace(anio, *evento);	// Añade el año y los eventos a la cronologia
			 delete evento;
	}
 
	return is;
 }

 	/**
 		* @brief Funcion que imprime la cronología
		* @param c Cronologia a imprimir
		* @param os Flujo de salida de datos
 	*/
	 ostream& operator<<(ostream &os, Cronologia &c){
		Cronologia::const_iterator it;
		for (it = c.begin(); it != c.end(); ++it) {
			os << (*it).first << "#";
			EventoHistorico::const_iterator it_ev;
			for (it_ev = (*it).second.begin(); it_ev != (*it).second.end(); ++it_ev) {
				os << (*it_ev) << "#";
			}

		}
	}