#include <string>
#include <fstream>
#include <map>
#include "Cronologia.h"
#include "EventoHistorico.h"

using namespace std;

void EscribirEnArchivo(string filename, string evento, int anio) {
	ofstream archivo;

	archivo.open(filename, std::fstream::out | std::fstream::app);
	archivo << anio << " # " << evento << endl;
	archivo.close();
}

void EscribirEnTerminal(string evento, int anio) {
	cout << "Año: " << anio << " # Evento: " << evento << endl;
}

void Intervalo(Cronologia & mycrono, int inicio, int fin, bool file, string filename) {
	Cronologia::iterator it_inicio, it_fin, itCrono;
	EventoHistorico::iterator itEventos;
	int year;

	it_inicio = mycrono.find(inicio);
	it_fin = mycrono.find(fin);

	for (itCrono = it_inicio; itCrono != it_fin; ++itCrono) {
		year = itCrono->first;
		for (itEventos = itCrono->second.begin(); itEventos != itCrono->second.end(); itEventos++) {
			if (file) {
				EscribirEnArchivo(filename, *itEventos, year);
			}
			else if (!file) {
				EscribirEnTerminal(*itEventos, year);
			}
		}	
	}
}


int main(int argc, char * argv[]) {
	string archivo_salida = "";
	Cronologia crono;
	bool fileoutput;
	
	int inicio, fin;

	
	ifstream archivo_entrada(argv[1]);
	
	if (argc != 5) {
		cout << "Error en los argumentos." << endl;
		cout << "Uso: ./filtradointervalo <archivo_cronologia> <año_inicio> <año_fin> <archivo_salida> " << endl;
		return 0;
	}

	if (argv[2] > argv[3]) {
		cout << "Error en los argumentos." << endl;
		cout << "Uso: ./filtradointervalo <archivo_cronologia> <año_inicio> <año_fin> <archivo_salida> " << endl;
		return 0;
	}

	if (!archivo_entrada) {
		cout << "Error al abrir el archivo." << argv[1] << endl;
		return 0;
	}

	inicio = atoi(argv[2]);
	fin = atoi(argv[3]);

	archivo_salida = argv[4];

	if (archivo_salida == "") {
		fileoutput = false;
	}
	else {
		fileoutput = true;
	}

	archivo_entrada >> crono;

	Intervalo(crono, inicio, fin, fileoutput, archivo_salida);
	
	return(0);
}
