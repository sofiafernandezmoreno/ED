#include <string>
#include <fstream>
#include "Cronologia.h"
#include "EventoHistorico.h"

using namespace std;

void EscribirEnArchivo(string filename, int part, int year_counter, int event_counter, int year, int max_year, int max_year_events, int media) {
	ofstream archivo;
	
	archivo.open(filename, std::fstream::out | std::fstream::app);
	
	if (part == 1) {
		archivo << "Análisis de la cronología.\n\n" << "Número total de años: " << year_counter << endl;
	}
	else if (part == 2) {
		archivo << "Recuento de eventos por año.\n";
		archivo << year << " # " << event_counter << " eventos." << endl;
	}
	else if (part == 3) {
		archivo << "\nMáximo de eventos." << endl;
		archivo << "El máximo de eventos lo tiene el año " << max_year << " con " << max_year_events << " eventos." << endl;
	}
	else if (part == 4) {
		cout << "\nMedia de eventos: " << media << endl;
	}

	archivo.close();
}

void EscribirEnTerminal(int part, int year_counter, int event_counter, int year, int max_year, int max_year_events, int media) {
	if (part == 1) {
		cout << "Análisis de la cronología.\n\n" << "Número total de años: " << year_counter << endl;
	}
	else if (part == 2) {
		cout << "Recuento de eventos por año.\n";
		cout << year << " # " << event_counter << " eventos." << endl;
	}
	else if (part == 3) {
		cout << "\nMáximo de eventos." << endl;
		cout << "El máximo de eventos lo tiene el año " << max_year << " con " << max_year_events << " eventos." << endl;
	}
	else if (part == 4) {
		cout << "\nMedia de eventos: " << media << endl;
	}
}

void Estadistica(Cronologia & mycrono, bool file, string filename) {
	int year_counter = 0, event_counter = 0, numero, first_year, last_year, part = 0, supercontador, year;
	int max_year = 0, max_year_events = 0;
	double media;
	Cronologia::iterator itCrono;
	EventoHistorico::iterator itEvento;

	// TODO
	// primer año y último de la cronologia
	
	first_year = mycrono.begin()->first;
	last_year = mycrono.end()->first;

	year_counter = mycrono.size();

	// Escritura de la primera parte de la estadistica
	part = 1;
	if (file) {
		EscribirEnArchivo(filename, part, year_counter, NULL, NULL, NULL, NULL, NULL);
	}
	else if (!file) {
		EscribirEnTerminal(part, year_counter, NULL, NULL, NULL, NULL, NULL);
	}

	// Contador de eventos por año.
	int recuento[year_counter], i = 0;

	part = 2;
	for (itCrono = mycrono.begin(); itCrono != mycrono.end(); ++itCrono) {
		year = itCrono->first;
		
		for (itEvento = itCrono->second.begin(); itEvento != itCrono->second.end(); itEvento++) {
			event_counter++;
		}

		recuento[i] = event_counter;

		if (event_counter > max_year_events) {
			max_year_events = event_counter;
			max_year = year;
		}

		if (file) {
			EscribirEnArchivo(filename, part, NULL, event_counter, year, NULL, NULL, NULL);
		}
		else if (!file) {
			EscribirEnTerminal(part, NULL, event_counter, year, NULL, NULL, NULL);
		}
		event_counter = 0;
		++i;
	}

	// Maximo de eventos en un año.
	part = 3;
	if (file) {
		EscribirEnArchivo(filename, part, NULL, NULL, NULL, max_year, max_year_events, NULL);
	}
	else if (!file) {
		EscribirEnTerminal(part, NULL, NULL, NULL, max_year, max_year_events, NULL);
	}

	// Media de eventos.
	part = 4;
	for (int i = 0; i < year_counter; ++i) {
		supercontador += recuento[i];
	}

	media = supercontador / year_counter;

	if (file) {
		EscribirEnArchivo(filename, part, NULL, NULL, NULL, NULL, NULL, media);
	}
	else if (!file) {
		EscribirEnTerminal(part, NULL, NULL, NULL, NULL, NULL, media);
	}
}


int main(int argc, char * argv[]) {
	string archivo_salida = "";
	Cronologia crono;
	bool fileoutput;
	
	int inicio, fin;

	
	ifstream archivo_entrada(argv[1]);
	
	if (argc != 3) {
		cout << "Error en los argumentos." << endl;
		cout << "Uso: ./estadisticaeventos <archivo_cronologia> <archivo_salida> " << endl;
		return 0;
	}

	if (!archivo_entrada) {
		cout << "Error al abrir el archivo: " << argv[1] << endl;
		return 0;
	}

	archivo_salida = argv[2];

	if (archivo_salida == "") {
		fileoutput = false;
	}
	else {
		fileoutput = true;
	}

	archivo_entrada >> crono;
	
	Estadistica(crono, fileoutput, archivo_salida);
	
	return(0);
}
