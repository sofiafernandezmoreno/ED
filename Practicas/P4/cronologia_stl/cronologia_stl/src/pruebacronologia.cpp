#include <fstream>
#include <iostream>
#include "Cronologia.h"
#include "EventoHistorico.h"
using namespace std;

int main(int argc, char * argv[]) {

	if (argc != 2) {
		cout << "Dime el nombre del fichero con la cronologia" << endl;
		return 0;
	}

	ifstream f(argv[1]);
	if (!f) {
		cout << "No puedo abrir el fichero " << argv[1] << endl;
		return 0;
	}

	Cronologia mi_cronologia;
	f >> mi_cronologia; //Cargamos en memoria, en el traductor.

	int anio;
	cout << "[+] Dime un año a consultar: ";
	cin >> anio;

	EventoHistorico eventos;
	eventos = mi_cronologia.GetEventos(anio);  //Asumimos que Cronologia::GetEventos() devuelve objeto de clase EventoHistorico


	// Recorremos con iterador los acontecimientos para mostrarlos por pantalla
	// Este proceso requiere la definici�n de un tipo iterator // const_iterator en EventoHistorico
	// Y la definici�n de los m�todos begin() y end() en EventoHistorico
	
	EventoHistorico::const_iterator it;
	int size = 0;

	for (it = eventos.begin(); it != eventos.end(); ++it) {
		size++;
	}

	cout << endl << " "<< anio << endl << "  │" << endl;
	for (it = eventos.begin(); it != eventos.end(); ++it) {
		if (size != 1) {
			cout << "  ├─ "<< (*it) << endl;
		}
		else if (size == 1) {
			cout << "  └─ "<< (*it) << endl;
		}
		size--;
	}
}
