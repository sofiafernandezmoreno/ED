#include <iostream>
#include <string>
#include <map>
#include <ostream>
#include <vector>
#include <istream>
#include <sstream>
#include <utility>	// pair
#include "EventoHistorico.h"

using namespace std;

EventoHistorico::EventoHistorico(){
}

EventoHistorico::EventoHistorico(int anio) {
	(*this).ev.first = anio;
}

EventoHistorico::~EventoHistorico() {
	(*this).ev.first = 0;
	(*this).ev.second.clear();
}

EventoHistorico::iterator EventoHistorico::begin() {
	return (*this).ev.second.begin();
}

EventoHistorico::const_iterator EventoHistorico::begin() const {
	return (*this).ev.second.cbegin();
}

EventoHistorico::iterator EventoHistorico::end() {
	return (*this).ev.second.end();
}

EventoHistorico::const_iterator EventoHistorico::end() const {
	return (*this).ev.second.cend();
}

void EventoHistorico::AniadeEventoHistorico(string evento) {
	(*this).ev.second.emplace(evento);

	vector<string> split_words = Separar(evento);

	for (unsigned int i = 0; i < split_words.size(); ++i) {
   	  if(PalabraEnRango(split_words.at(i))) {
     	  (*this).palabras_clave.emplace(split_words.at(i));
    	}
  }
   split_words.clear();
}

vector<string> EventoHistorico::Separar(const string &s) {
    istringstream iss(s);
    string item;                             // Separa las palabras del string que le entra
    vector<string> palabras;
    while (iss) {
        string word;
        iss >> word;
        palabras.push_back(word);
    }
    return palabras;
}

bool EventoHistorico::PalabraEnRango(const string palabra) {
   int tam = palabra.length();
   bool test, out_BL;

   for (unsigned int i = 0; i < (*this).blackListed.size(); ++i) {
      if((*this).blackListed.at(i) == palabra) {
         out_BL = false;
      }
      else {
         out_BL = true;
      }
   }

   if(tam >= 4 && tam <= 12 && out_BL) {
      test = true;
   }
   else {
      test = false;
   }

   return test;
}

int EventoHistorico::getanio(){
    return (*this).ev.first;
  }
  
  void EventoHistorico::setanio(int a){
    (*this).ev.first = a;
  }
  
  set<string> EventoHistorico::getsucesos(){
    return (*this).ev.second;
  }
  
  void EventoHistorico::insertasuceso(string s){
    (*this).ev.second.insert(s);
  }
  
  string EventoHistorico::buscasuceso (string& s) {
      set<string>::iterator it;
  
      for (it=(*this).ev.second.begin(); it!=(*this).ev.second.end(); ++it){
          if (s == *it)
              return *it;
      }
  
      s = "No está" ;
  
      return s ;
  }

