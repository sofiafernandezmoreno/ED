#include <string>
#include <fstream>
#include <map>
#include "Cronologia.h"
#include "EventoHistorico.h"

using namespace std;

void EscribirEnArchivo(string filename, string evento, int anio) {
	ofstream archivo;

	archivo.open(filename, std::fstream::out | std::fstream::app);
	archivo << anio << " # " << evento << endl;
	archivo.close();
}

void EscribirEnTerminal(string evento, int anio) {
	cout << "A�o: " << anio << " # Evento: " << evento << endl;
}

void BusquedaPalabra(Cronologia & mycrono, string palabra, bool file, string filename) {
	Cronologia::iterator itCrono;
	EventoHistorico::iterator itEventos;
	int year;

	for (itCrono = mycrono.begin(); itCrono != mycrono.end(); ++itCrono) {
		year = itCrono->first;
		for (itEventos = itCrono->second.begin(); itEventos != itCrono->second.end(); itEventos++) {
			if (file) {
				EscribirEnArchivo(filename, *itEventos, year);
			}
			else if (!file) {
				EscribirEnTerminal(*itEventos, year);
			}	
		}	
	}
}

int main(int argc, char * argv[]) {
	string archivo_salida = "";
	Cronologia crono;
	bool fileoutput;
	string palabra;
	
	ifstream archivo_entrada(argv[1]);
	
	if (argc != 4) {
		cout << "Error en los argumentos." << endl;
		cout << "Uso: ./filtradopalabrasclave <archivo_entrada> <palabra> <archivo_salida> " << endl;
		return 0;
	}

	if (!archivo_entrada) {
		cout << "Error al abrir el archivo." << argv[1] << endl;
		return 0;
	}
	
	palabra = argv[2];
	archivo_salida = argv[3];

	if (archivo_salida == "") {
		fileoutput = false;
	}
	else {
		fileoutput = true;
	}

	archivo_entrada >> crono;

	BusquedaPalabra(crono, palabra, fileoutput, archivo_salida);
	
	return(0);
}
