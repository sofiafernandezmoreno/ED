#include <fstream>
#include <iostream>
#include "Cronologia.h"

using namespace std;

// Esta funcion también puede implementarse como m�todo de la clase Cronologia (A vuestra elecci�n)
Cronologia Union(const Cronologia& c1, const Cronologia& c2) {
	Cronologia result(c1);
	EventoHistorico* eventos;
	int anio;
	EventoHistorico::const_iterator itEvent;
	Cronologia::const_iterator itCrono;

	for (itCrono = c2.begin(); itCrono != c2.end(); itCrono++) {
		anio = itCrono->first;
		eventos = new EventoHistorico(anio);
		for (itEvent = itCrono->second.begin(); itEvent != itCrono->second.end(); itEvent++) {
			eventos->AniadeEventoHistorico(*itEvent);
		}
		result.emplace(anio, *eventos);
	}

	return result;
}
/*
// Este método tambi�n puede implementarse como operator<< asociado a la clase Cronolog�a (A vuestra elecci�n).
void ImprimeCronologia(const Cronologia &c, ostream &os) {
	Cronologia::const_iterator it;
	for (it = c.begin(); it != c.end(); ++it) {
		os << it->first << "#";          //año esta en el key del map
		EventoHistorico::const_iterator it_ev;
		for (it_ev = it->second.begin(); it_ev != it->second.end(); ++it_ev) {
			os << *it_ev << "#";
		}
		os << endl;
	}
}
*/
int main(int argc, char * argv[]) {

	if (argc != 3 && argc != 4) {
		cout << "Error: debe dar al menos los nombres de dos ficheros con cronolog�as. " << endl;
		cout << "[Opcional]: un tercer nombre de fichero para guardar la cronolog�a resultante." << endl;
		return 0;
	}

	ifstream f1(argv[1]);
	if (!f1) {
		cout << "No puedo abrir el fichero " << argv[1] << endl;
		return 0;
	}
	ifstream f2(argv[2]);
	if (!f2) {
		cout << "No puedo abrir el fichero " << argv[2] << endl;
		return 0;
	}

	Cronologia c1, c2, cUnion;
	f1 >> c1;    // Cargamos los datos de los ficheros en las cronolog�as.
	f2 >> c2;

	cUnion = Union(c1, c2);

	if (argc == 3)   
		cout<<cUnion;
	else {
		ofstream fout(argv[3]);
		if (!fout) {
			cout << "No puedo crear el fichero " << argv[3] << endl;
			return 0;
		}
		fout<<cUnion;

	}
}
