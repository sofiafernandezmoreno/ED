#ifndef _EventoHistorico_
#define _EventoHistorico_

#include <iostream>
#include <string>
#include <map>
#include <set>
#include <utility>	
#include <ostream>
#include <istream>
#include <vector>

using namespace std;

/**
	* @brief T.D.A. EventoHistorico
	*
	* Ejemplo de uso:
	* @include pruebacronologia.cpp
	*
	* @author Sofia Fernandez Moreno
	* @date Diciembre 2017
 */
class EventoHistorico {
private:
	/**
		* @page Conjunto Rep del TDA EventoHistoricoHistorico.
		*
		* @section Función de abstracción
		*
		* Un objeto válido del TDA EventoHistoricoHistorico representa:
		*
		* EventoHistoricoHistoricos asociados a un año
		*
	 */
	 /**
      * @brief Elementos privados del TDA EventoHistoricoHistorico.
      * @param ev Pair de entero y set de string para gusradar EventoHistoricoHistoricos asociados a años
      * @param palabras_clave Palabras clave para buscar rapidamente EventoHistoricoHistoricos
      * @param blacklisted Palabras no importantes y no claves
     */
	pair<int, set<string> > ev;
	set<string> palabras_clave;
	vector<string> blackListed  = {"the", "and", "for", "was", "our", "its", "how", "when", "were", "where"};

	vector<string> Separar(const string &s);
	bool PalabraEnRango(const string palabra);

public:
	/**
		 * @brief Elementos publicos del TDA EventoHistoricoHistorico.
		 * @param iterator iterador para el EventoHistoricoHistorico
		 * @param const_iterator iterador constante para el EventoHistoricoHistorico
		*/
	typedef typename set<string>::iterator iterator;
	typedef typename set<string>::const_iterator const_iterator;

	

 /**
   * @brief Constructor con año para el EventoHistorico.
	 * @param anio Año con el que inicializar el EventoHistorico
  */
	EventoHistorico(const int anio);

	/**
		* @brief Constructor por defecto del EventoHistorico.
	*/
	EventoHistorico();

	/**
    * @brief Destructor del EventoHistorico.
   */
	~EventoHistorico();

	/**
    * @brief Funcion que devuelve un iterador al principio del EventoHistorico
  */
	iterator begin();
	const_iterator begin() const;

	/**
    * @brief Funcion que devuelve un iterador al final del EventoHistorico
   */
	iterator end();
	const_iterator end() const;

	/**
    * @brief Funcion que añade un EventoHistorico
    * @param EventoHistorico String que contiene el enunciado de un EventoHistorico
   */
	void AniadeEventoHistorico(string EventoHistorico);

	/**
    * @brief Funcion que devuelve el año del evento
    * @return Año
   */
	int getanio();
   /**
    * @brief Funcion que asigna un año a un evento
    * @param a Int que contiene el año
   */  
	void setanio(int a);
	 /**
    * @brief Funcion que devuelve un conjunto de sucesos
    * @return set<string>
   */ 
	set<string> getsucesos();
		
	   /**
    * @brief Funcion que inserta un suceso al conjunto de sucesos
    * @param s String que contiene un suceso
   */
	  void insertasuceso(string s);
	/**
    * @brief Funcion que busca un suceso concreto
	* @param s String que contiene un suceso
	* @return string
   */
	  string buscasuceso (string& s);
	  
};

#endif
