#ifndef _CRONOLOGIA_
#define _CRONOLOGIA_

#include <iostream>
#include <string>
#include <map>
#include <ostream>
#include <istream>
#include "EventoHistorico.h"

using namespace std;

/**
	* @brief T.D.A. Cronología.
	*
	* Ejemplo de uso:
	* @include pruebacronologia.cpp
	*
	* @author Sofia Fernandez Moreno
	* @date Diciembre 2017
 */
class Cronologia {
private:
	/**
		* @page Conjunto Rep del TDA Cronología.
		*
		* @section Función de abstracción
		*
		* Un objeto válido del TDA Cronología representa:
		*
		* Eventos asociados a un año
		*
	 */
 /**
    * @brief Elementos privados del TDA Cronología.
    * @param mapa Mapa de entero que guarda el año y Eventos
   */
	map<int, EventoHistorico> mapa;

public:
	/**
	 * @brief Elementos publicos del TDA Cronología.
	 * @param iterator iterador para la cronología
	 * @param const_iterator iterador constante para la cronología
	*/
	typedef typename map<int, EventoHistorico>::iterator iterator;
	typedef typename map<int, EventoHistorico>::const_iterator const_iterator;



	/**
 		* @brief Constructor por defecto de la cronología
 	*/
	Cronologia();

	/**
 		* @brief Constructor de copia de la cronología
		* @param otra Cronología a copiar.
 	*/
	Cronologia(const Cronologia& otra);
	/**
 		* @brief Detructor de la cronología
 	*/
	~Cronologia();

	/**
 		* @brief Funcion que devuelve un iterador al principio de la cronologia
		* @retval Iterador al principio de la cronología
 	*/
	iterator begin();
	const_iterator begin() const;

	/**
 		* @brief Funcion que devuelve un iterador al final de la cronologia
		* @retval Iterador al final de la cronología
 	*/
	iterator end();
	const_iterator end() const;



	/**
 		* @brief Funcion que devuelve el tamaño de la cronología
		* @retval tamaño de la cronología
 	*/
	int size();

	/**
 		* @brief Funcion que devuelve si la cronología esta vacia
		* @retval si esta vacia o no
 	*/
	bool empty();

	/**
 		* @brief Funcion que devuelve los eventos en un año
		* @param year año a consultar
		* @retval eventos
 	*/
	EventoHistorico at(int year);

	/**
 		* @brief Funcion que añade sucesos en un año
		* @param anio año del evento
		* @evento evento
 	*/
	void emplace(int anio, EventoHistorico evento);

	/**
 		* @brief Funcion que borra los eventos en un año
		* @param anio año a borrar
 	*/
	void erase(int anio);

	/**
 		* @brief Funcion que busca un año
		* @param year año a buscar
		* @retval iterador donde esta ese año en la cronología
 	*/
	iterator find(int year);

	/**
 		* @brief Funcion que devuelve los eventos en un año
		* @param year año a consultar
		* @retval eventos
 	*/
	EventoHistorico GetEventos(int year);

	
};

/**
* @brief Entrada de una cronología desde istream.
* @param in stream de entrada
* @param cronologia Cronologia que recibe los años y eventos.
* @retval La cronología introducida en r
* @pre La entrada tiene el formato año, que se introduce en el vector de años, y eventos, que se introducen en el vector de eventos.
*/
istream& operator>>(istream& is, Cronologia &cronologia);
/**
* @brief Salida de una cronología desde ostream.
* @param out stream de salida
* @param cronologia Cronologia que recibe los años y eventos.
* @retval La cronología introducida en r
* @pre La salida tiene el formato año, que se introduce en el vector de años, y eventos, que se introducen en el vector de eventos.
*/
ostream& operator<<(ostream &os, Cronologia &c);

#endif
