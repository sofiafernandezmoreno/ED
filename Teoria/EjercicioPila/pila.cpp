/**
  * @file pila.cpp
  * @brief Implementaci�n del TDA Pila
  *
  */
#include <cassert>
// #include <pila.h>  El codigo ya se incluye en pila.h

/* _________________________________________________________________________ */
/**
 * @brief A�ade un elemento "encima" del tope de la pila
 * @param elem Elemento que se va a a�adir.
 */
 template <class T>
void Pila_Doble<T>::push(int numpila, const T & elem){

}
/**
 * @brief Quita el elemento del tope de la pila
 */
 template <class T>
void Pila_Doble<T>::pop(int numpila){
  top[numpila]=top[numpila]-(numpila?1:-1)
}


template <class T>
void Pila<T>::poner(const T & elem){
  primera = new Celda(elem,primera); //Creamos un nuevo nodo en el tope
  num_elem++;                        //Actualizamos num_elem
}

/* _________________________________________________________________________ */

template <class T>
void Pila<T>::quitar(){
  assert(primera!=0);           //Si la pila no tiene elementos, abortar
  Celda *p = primera;           //Copiamos el puntero al tope
  primera = primera->siguiente; //Actualizamos primera
  delete p;                     //Borramos el nodo que ocupaba el tope
  num_elem--;                   //Actualizamos num_elem
}
